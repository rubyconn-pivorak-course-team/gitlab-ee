require 'spec_helper'

describe Gitlab::QuickActions::SpendTimeParser do
  subject { described_class }

  shared_examples :command_with_invalid_arguments do
    it 'returns nil' do
      expect(subject.new(arguments).execute).to eq(nil)
    end
  end

  shared_examples :command_with_valid_arguments do
    let(:expected_response) do
      {
        time:    parsed_time,
        date:    parsed_date,
        message: parsed_message
      }
    end

    it 'returns proper hash' do
      expect(subject.new(arguments).execute).to eq(expected_response)
    end
  end

  describe '#execute' do
    context 'receives invalid command arguments' do
      context 'with empty line' do
        include_examples :command_with_invalid_arguments do
          let(:arguments) { '' }
        end
      end

      context 'without time' do
        include_examples :command_with_invalid_arguments do
          let(:arguments) { '2018-01-01 "Message"' }
        end
      end

      context 'with invalid date' do
        context 'without year' do
          include_examples :command_with_invalid_arguments do
            let(:arguments) { '2h -01-01 "Message"' }
          end
        end

        context 'without month' do
          include_examples :command_with_invalid_arguments do
            let(:arguments) { '2h 18--01 "Message"' }
          end
        end

        context 'without day' do
          include_examples :command_with_invalid_arguments do
            let(:arguments) { '2h 18-01- "Message"' }
          end
        end

        context 'with invalid separator' do
          include_examples :command_with_invalid_arguments do
            let(:arguments) { '2h 2018.01-01 "Message"' }
          end
        end

        context 'with invalid month' do
          include_examples :command_with_invalid_arguments do
            let(:arguments) { '2h 2018-13-01 "Message"' }
          end
        end
      end

      context 'with trash' do
        include_examples :command_with_invalid_arguments do
          let(:arguments) { 'dfjkghdskjfghdjskfgdfg' }
        end
      end
    end

    context 'receives valid command arguments' do
      context 'with time only' do
        include_examples :command_with_valid_arguments do
          let(:arguments)      { '2h' }
          let(:parsed_time)    { Gitlab::TimeTrackingFormatter.parse(arguments) }
          let(:parsed_date)    { DateTime.now.to_date }
          let(:parsed_message) { nil }
        end
      end

      context 'with time and date, without message' do
        let(:time)           { '10m' }
        let(:arguments)      { "#{time} #{date}" }
        let(:parsed_date)    { Date.parse(date) }
        let(:parsed_time)    { Gitlab::TimeTrackingFormatter.parse(time) }
        let(:parsed_message) { nil }

        context 'with - date separator' do
          include_examples :command_with_valid_arguments do
            let(:date) { '2018-01-01' }
          end
        end

        context 'with / date separator' do
          include_examples :command_with_valid_arguments do
            let(:date) { '2018/01/01' }
          end
        end

        context 'with . date separator' do
          include_examples :command_with_valid_arguments do
            let(:date) { '2018.01.01' }
          end
        end
      end

      context 'with time and message, without date' do
        let(:time)           { '2h' }
        let(:arguments)      { "#{time} #{message}" }
        let(:parsed_date)    { DateTime.now.to_date }
        let(:parsed_time)    { Gitlab::TimeTrackingFormatter.parse(time) }
        let(:parsed_message) { parse_message }

        context 'when message in double quotes' do
          include_examples :command_with_valid_arguments do
            let(:message) { '"Message"' }
          end
        end

        context 'when message in single quotes' do
          include_examples :command_with_valid_arguments do
            let(:message) { "'Message'" }
          end
        end
      end

      context 'with time, date and message' do
        let(:time)           { '2h' }
        let(:date)           { '2018-01-01' }
        let(:message)        { '"Message"' }
        let(:parsed_date)    { Date.parse(date) }
        let(:parsed_time)    { Gitlab::TimeTrackingFormatter.parse(time) }
        let(:parsed_message) { parse_message }

        context 'when "time...date...message"' do
          include_examples :command_with_valid_arguments do
            let(:arguments) { "#{time} #{date} #{message}" }
          end
        end

        context 'when "date...time...message"' do
          include_examples :command_with_valid_arguments do
            let(:arguments) { "#{date} #{time} #{message}" }
          end
        end

        context 'when "date...message...time"' do
          include_examples :command_with_valid_arguments do
            let(:arguments) { "#{date} #{message} #{time}" }
          end
        end

        context 'when "message...date...time"' do
          include_examples :command_with_valid_arguments do
            let(:arguments) { "#{message} #{date} #{time}" }
          end
        end
      end

      def parse_message
        matched_message = arguments.match(subject::MESSAGE_REGEX)
        matched_string  = matched_message.try(:[], 0)

        matched_string&.delete("'\"")
      end
    end
  end
end
