module Emails
  module CsvExport
    def issues_csv_email(user, project, *csv_services)
      @project      = project
      @csv_services = csv_services

      csv_services.each do |service|
        filename = generate_issues_filename(project.full_path.parameterize, service.postfix)
        attachments[filename] = issues_attachment(service.csv_data)
      end

      send_mail(user.notification_email)
    end

    private

    def send_mail(email)
      mail(to: email, subject: subject("Exported issues")) do |format|
        format.html { render layout: 'mailer' }
        format.text
      end
    end

    def generate_issues_filename(project_name, postfix)
      result_name = "#{project_name}_issues"
      result_name << "_#{postfix}" if postfix.present?
      result_name << "_#{Date.today.iso8601}.csv"
    end

    def issues_attachment(data)
      {
        content: data,
        mime_type: 'text/csv'
      }
    end
  end
end
