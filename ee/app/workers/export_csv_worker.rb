class ExportCsvWorker
  include ApplicationWorker

  def perform(current_user_id, project_id, params)
    params[:project_id] = project_id

    current_user = User.find(current_user_id)
    project      = Project.find(project_id)
    @issues      = IssuesFinder.new(current_user, params.symbolize_keys).execute

    Notify.issues_csv_email(current_user, project, general_service, timelogs_service).deliver_now
  end

  private

  def general_service
    Issues::ExportCsvService.new(@issues)
  end

  def timelogs_service
    Issues::ExportTimelogCsvService.new(@issues)
  end
end
