module Issues
  class ExportTimelogCsvService < Issues::ExportCsvService
    POSTFIX = 'with_timelogs'.freeze

    def csv_builder
      @csv_builder ||=
        ExtraLineCsvBuilder.new(
          @issues.includes(:author, :assignees, :timelogs),
          all_header_to_value_hash,
          :timelogs,
          repeatable_header_to_value_hash
        )
    end

    private

    def repeatable_header_to_value_hash
      {
        'Time Spent'    => -> (issue, index = 0) { issue.timelogs[index].time_spent if timelog_in_range?(issue, index) },
        'Time Spent On' => -> (issue, index = 0) { issue.timelogs[index].spent_at || issue.timelogs.first.created_at if timelog_in_range?(issue, index) },
        'Time Spent By' => -> (issue, index = 0) { issue.timelogs[index].user.username if timelog_in_range?(issue, index) },
        'Comment'       => -> (issue, index = 0) { issue.timelogs[index].message if timelog_in_range?(issue, index) }
      }
    end

    def all_header_to_value_hash
      header_to_value_hash.merge(repeatable_header_to_value_hash)
    end

    def timelog_in_range?(issue, index)
      issue.timelogs.size > index
    end

    def time_spent
      -> (issue, index = 0) { issue.timelogs[index].time_spent if timelog_in_range?(issue, index) }
    end

    def time_spent_on
      -> (issue, index = 0) { issue.timelogs[index].spent_at || issue.timelogs.first.created_at if timelog_in_range?(issue, index) }
    end

    def time_spent_by
      -> (issue, index = 0) { issue.timelogs[index].user.username if timelog_in_range?(issue, index) }
    end
  end
end
