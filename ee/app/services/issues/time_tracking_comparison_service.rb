module Issues
  class TimeTrackingComparisonService
    HUMAN_NO_TIME = '--'.freeze

    def initialize(issue)
      @issue = issue
    end

    def call
      "#{spent} / #{estimate}" if spent_or_estimate?
    end

    private

    attr_reader :issue

    delegate :human_total_time_spent, :human_time_estimate, to: :issue

    def spent_or_estimate?
      human_total_time_spent.present? || human_time_estimate.present?
    end

    def spent
      human_total_time_spent.presence || HUMAN_NO_TIME
    end

    def estimate
      human_time_estimate.presence || HUMAN_NO_TIME
    end
  end
end
