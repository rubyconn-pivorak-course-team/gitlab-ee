# This class get a hash of general headers
# and fields that should be written in extra lines
# and generate a special header for such fields
# and write data for one object from many relation
# As the params for writting row we take object from which
# we should get a relation value for example issue
# and index of relation object
# we take such arguments to don't break constructions
# of headers what is already used
# Example:
#      columns = {
#       'Title' => 'title',
#       'Comment' => 'comment',
#       'Author' => -> (post) { post.author.full_name }
#       'Created At (UTC)' => -> (post) { post.created_at&.strftime('%Y-%m-%d %H:%M:%S') }
#       'Time Spent By' => -> (issue, index = 0) { issue.timelogs[index].user.username }
#      }
#
#      repeatable_fields = {
#        'Time Spent By' => -> (issue, index = 0) { issue.timelogs[index].user.username }
#      }
# ExtraLineBuilder.new(columns, repeatable_fields).row_for(Issue.first, 2)
# We get(without headers):
#   Title   Comment   Author   Create At    Time Spent By
#    nil      nil      nil        nil         LockiStrike
class ExtraLineBuilder
  include ExcelSanitize

  def initialize(header_to_value_hash, repeatable_fields)
    @header_to_value_hash = header_to_value_hash
    @repeatable_fields = repeatable_fields
  end

  def row_for(object, index)
    headers.map do |column|
      if repeatable_headers.include?(column)
        excel_sanitize(@repeatable_fields[column].call(object, index))
      end
    end
  end

  private

  def headers
    @headers ||= @header_to_value_hash.keys
  end

  def repeatable_headers
    @repeatable_headers ||= @repeatable_fields.keys
  end
end
