module ExcelSanitize
  module_function

  def excel_sanitize(line)
    return if line.nil?

    line.prepend("'") if line =~ /^[=\+\-@;]/
    line
  end
end
