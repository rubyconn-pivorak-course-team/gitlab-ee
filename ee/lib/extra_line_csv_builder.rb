# This class work like CsvBuilder
# but can generate a special lines
# where you want to write only some columns from whole headers
# For example you have many time tracking records in issue
# you write in first line all info about issue and first time tracking record
# and in next lines only time tracking records without issue info
# It looks like:
# col1  col2  col3  col4
# val11 val12 val13 val141
#                   val142
#                   val143
#                   val144
# val21 val22 val23 val241
#                   val242
#
# Example of usage:
# ExtraLineCsvBuilder.new(Issue.all, headers, :symbolyzed_association_to_repeat, repeatable_headers).render
# Example of headers:
#     columns = {
#       'Title' => 'title',
#       'Comment' => 'comment',
#       'Author' => -> (post) { post.author.full_name }
#       'Created At (UTC)' => -> (post) { post.created_at&.strftime('%Y-%m-%d %H:%M:%S') }
#     }

class ExtraLineCsvBuilder < CsvBuilder
  EXTRA_ASSOCIATED_OBJECTS_LIMIT = 1
  START_EXTRA_OBJECTS_INDEX = 1

  def initialize(collection, header_to_value_hash, associated_objects_name, repeatable_fields)
    super(collection, header_to_value_hash)

    @associated_objects_name = associated_objects_name
    @repeatable_fields = repeatable_fields
    @extra_line_builder = ExtraLineBuilder.new(header_to_value_hash, repeatable_fields)
  end

  def rows_expected
    if truncated? || rows_written == 0
      prewrite_rows_expectation
    else
      rows_written
    end
  end

  private

  def associated_objects(object)
    object.public_send(@associated_objects_name) # rubocop:disable GitlabSecurity/PublicSend
  end

  def collect_rows(csv, object, &until_condition)
    super(csv, object)

    write_extra_rows(csv, object, &until_condition) if extra_associated_objects?(object)
  end

  def extra_associated_objects?(object)
    associated_objects(object).size > EXTRA_ASSOCIATED_OBJECTS_LIMIT
  end

  def write_extra_rows(csv, object)
    START_EXTRA_OBJECTS_INDEX.upto(associated_objects(object).size - 1) do |index|
      csv << @extra_line_builder.row_for(object, index)

      @rows_written += 1

      if yield
        @truncated = true
        break
      end
    end
  end

  def prewrite_rows_expectation
    # get size of all non-empty relations
    result = @collection.flat_map(&@associated_objects_name).size
    # add to counter all empty([]) relations which flat_map doesn't include
    @collection.each { |c| result += 1 if associated_objects(c).empty? }

    result
  end
end
