require 'spec_helper'
require 'email_spec'

describe Emails::CsvExport do
  include EmailSpec::Matchers
  include_context 'gitlab email notification'

  describe 'csv export email' do
    shared_context :define_services do |truncated = false, rows_expected = 10, rows_written = 10|
      let(:general_service) do
        double(
          :general_service,
          csv_builder: double(
            :general_builder,
            truncated?:    truncated,
            rows_expected: rows_expected,
            rows_written:  rows_written
          ),
          csv_data: 'dummy content',
          postfix:  ::Issues::ExportCsvService::POSTFIX
        )
      end

      let(:timelogs_service) do
        double(
          :timelogs_service,
          csv_builder: double(
            :timelogs_builder,
            truncated?:    truncated,
            rows_expected: rows_expected,
            rows_written:  rows_written
          ),
          csv_data: 'timelogs dummy content',
          postfix:  ::Issues::ExportTimelogCsvService::POSTFIX
        )
      end
    end

    let(:user)          { create(:user) }
    let(:empty_project) { create(:project, path: 'myproject') }

    include_context :define_services

    subject { Notify.issues_csv_email(user, empty_project, general_service, timelogs_service) }

    let(:attachment)         { subject.attachments.first }
    let(:timelog_attachment) { subject.attachments.second }

    shared_examples :check_filename do |attachment, service|
      it 'generates a useful filename' do
        expect(public_send(attachment).filename).to include('myproject')
        expect(public_send(attachment).filename).to include('issues')
        expect(public_send(attachment).filename).to include(service::POSTFIX)
        expect(public_send(attachment).filename).to include(Date.today.year.to_s)
        expect(public_send(attachment).filename).to end_with('.csv')
      end
    end

    include_examples :check_filename, :attachment,         ::Issues::ExportCsvService
    include_examples :check_filename, :timelog_attachment, ::Issues::ExportTimelogCsvService

    it 'attachment has csv mime type' do
      subject.attachments.each do |attachment|
        expect(attachment.mime_type).to eq 'text/csv'
      end
    end

    it 'mentions project name' do
      expect(subject).to have_content empty_project.name
    end

    it "doesn't need to mention truncation by default" do
      expect(subject).not_to have_content 'truncated'
    end

    context 'when truncated' do
      include_context :define_services, true, 50, 42

      it 'mentions that the csv has been truncated' do
        expect(subject).to have_content 'truncated'
      end

      it 'mentions the number of issues written and expected' do
        expect(subject).to have_content '42 of 50 issues'
      end
    end
  end
end
