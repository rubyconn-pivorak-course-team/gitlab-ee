require 'spec_helper'

describe Issues::ExportTimelogCsvService do
  SPENT_VALUES = [
    SPENT_TIME_1 = '360'.freeze,
    SPENT_TIME_2 = '200'.freeze
  ].freeze

  SPENT_AT_DATES = [
    SPENT_AT_DATE_1 = '2016-05-05'.freeze,
    SPENT_AT_DATE_2 = '2012-12-12'.freeze
  ].freeze

  let(:user) { create(:user) }
  let(:project) { create(:project, :public) }
  let(:issue) { create(:issue, project: project, author: user) }
  let(:subject) { described_class.new(Issue.all) }

  context 'includes' do
    before do
      issue.timelogs.create(time_spent: SPENT_TIME_1, user: user, spent_at: SPENT_AT_DATE_1)
      issue.timelogs.create(time_spent: SPENT_TIME_2, user: user, spent_at: SPENT_AT_DATE_2)
    end

    def csv
      CSV.parse(subject.csv_data, headers: true)
    end

    context 'includes' do
      it 'Time Spent field' do
        expect(csv['Time Spent'].sort).to eq SPENT_VALUES.sort
      end

      it 'Time Spent By field' do
        csv.each do |line|
          expect(line['Time Spent By']).to eq user.username
        end
      end

      it 'Time Spent On field' do
        target_array = SPENT_AT_DATES.map { |date| Date.parse(date).beginning_of_day.to_s }

        expect(csv['Time Spent On'].sort).to eq target_array.sort
      end
    end

    context 'second and next issue rows don\'t include' do
      it 'common fields' do
        %w(Title Author State).each do |field|
          expect(csv[1][field]).to eq nil
        end
      end
    end
  end
end
