require 'spec_helper'

describe Issues::TimeTrackingComparisonService do
  let(:service) { described_class.new(issue)}

  shared_examples 'invoke call' do
    before do
      allow(issue).to receive(:time_estimate).and_return(time_estimate)
      allow(issue).to receive(:total_time_spent).and_return(total_time_spent)
    end

    let!(:issue) { create(:issue) }
    let(:comparison) { service.call }

    it 'as result return string' do
      expect(comparison).to be_an_instance_of(String)
    end

    it 'result include' do
      expect(comparison).to eq(result)
    end
  end

  describe '#call' do
    context 'time_spent present and not zero' do
      context 'time_estimate present and not zero' do
        include_examples 'invoke call' do
          let(:total_time_spent) { 1500 }
          let(:time_estimate) { 3000 }
          let(:result) { "#{issue.human_total_time_spent} / #{issue.human_time_estimate}" }
        end
      end

      context 'time_estimate absent or equal zero' do
        include_examples 'invoke call' do
          let(:total_time_spent) { 1500 }
          let(:time_estimate) { 0 }
          let(:result) { "#{issue.human_total_time_spent} / --" }
        end
      end
    end

    context 'time_estimate present and not zero' do
      context 'time_spent absent or equal zero' do
        include_examples 'invoke call' do
          let(:total_time_spent) { 0 }
          let(:time_estimate) { 3000 }
          let(:result) { "-- / #{issue.human_time_estimate}" }
        end
      end
    end

    context 'time_estimate and time_spent are absent' do
      before do
        allow(issue).to receive(:time_estimate).and_return(0)
        allow(issue).to receive(:total_time_spent).and_return(0)
      end

      let!(:issue) { create(:issue) }

      it 'as result return nil' do
        expect(service.call).to be_nil
      end
    end
  end
end
