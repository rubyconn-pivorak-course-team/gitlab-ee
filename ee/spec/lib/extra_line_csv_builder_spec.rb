require 'spec_helper'

describe ExtraLineCsvBuilder do
  let(:user) { create(:user) }
  let(:project) { create(:project, path: 'myproject') }
  let(:issue) { create(:issue, project: project, author: user) }
  let!(:second_issue) { create(:issue, project: project, author: user) }
  let(:header_to_value_hash) do
    {
      'Issue ID' => 'iid',
      'URL' => -> (issue) { issue_url(issue) },
      'Title' => 'title',
      'State' => -> (issue) { issue.closed? ? 'Closed' : 'Open' },
      'Description' => 'description',
      'Time Spent'    => -> (issue, index = 0) { issue.timelogs[index].time_spent if timelog_in_range?(issue, index) },
      'Time Spent On' => -> (issue, index = 0) { issue.timelogs[index].spent_at.to_date || issue.timelogs.first.created_at if timelog_in_range?(issue, index) }
    }
  end
  let(:repeatable_fields) do
    {
      'Time Spent'    => -> (issue, index = 0) { issue.timelogs[index].time_spent if timelog_in_range?(issue, index) },
      'Time Spent On' => -> (issue, index = 0) { issue.timelogs[index].spent_at.to_date || issue.timelogs.first.created_at if timelog_in_range?(issue, index) }
    }
  end
  let(:timelogs) { :timelogs }
  let(:subject) do
    described_class.new(
      Issue.all,
      header_to_value_hash,
      timelogs,
      repeatable_fields
    )
  end
  let(:test) { subject.render }

  before do
    issue.timelogs.create(time_spent: 360, user: user, spent_at: '2016-05-05')
    issue.timelogs.create(time_spent: 200, user: user, spent_at: '2012-12-12')
    issue.timelogs.create(time_spent: 2500, user: user, spent_at: '2000-01-01')
    second_issue.timelogs.create(time_spent: 777, user: user, spent_at: '2004-06-22')
    second_issue.timelogs.create(time_spent: 23, user: user, spent_at: '2007-10-27')
  end

  describe '#rows_expected' do
    it 'calculates expectation of rows considering relation object count' do
      expect(subject.rows_expected).to eq 5
    end
  end

  def csv
    CSV.parse(subject.render, headers: true)
  end
end
