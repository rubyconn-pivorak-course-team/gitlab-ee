require 'spec_helper'

describe ExtraLineBuilder do
  let(:header_to_value_hash) do
    {
      col1: -> { 'column1' },
      col2: -> (object, index) { "column2 with #{object}, #{index}" },
      col3: -> { 'column3' },
      col4: -> (object, index) { "column4 with #{object}, #{index}" }
    }
  end
  let(:repeatable_fields) do
    {
      col2: -> (object, index) { "column2 with #{object}, #{index}" },
      col4: -> (object, index) { "column4 with #{object}, #{index}" }
    }
  end
  let(:object) { create(:issue) }
  let(:index) { 3 }
  let(:correct_row) do
    [
      nil,
      repeatable_fields[:col2].call(object, index),
      nil,
      repeatable_fields[:col4].call(object, index)
    ]
  end
  let(:subject) { described_class.new(header_to_value_hash, repeatable_fields) }
  let(:subject_with_no_coincidence) { described_class.new(header_to_value_hash, {}) }

  describe '#row_for' do
    it 'creates correct row' do
      expect(subject.row_for(object, index)).to eq correct_row
    end

    it 'works with no coincidence' do
      expect(subject_with_no_coincidence.row_for(object, index)).to eq Array.new(header_to_value_hash.keys.size)
    end
  end
end
