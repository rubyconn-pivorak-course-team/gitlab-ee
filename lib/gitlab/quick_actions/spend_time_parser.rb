# This class takes spend command argument
# and parses time, date and message parts
# Example:
# spend_command_arguments = "1d 2h 3m 2018-07-19 'SpendTimeParser refactored'"
# SpendTimeParser.new(spend_command_arguments).execute
# => {:time=>36180, :date=>Thu, 19 Jul 2018, :message=>"SpendTimeParser refactored"}
module Gitlab
  module QuickActions
    class SpendTimeParser
      include Gitlab::Utils::StrongMemoize

      DATE_REGEX    = %r{(\d{2,4}[/\-.]\d{1,2}[/\-.]\d{1,2})}
      MESSAGE_REGEX = /(["'])(?:(?=(\\?))\2.)*?\1/
      USER_REGEX    = /@[^\s]+/

      def initialize(command_arguments)
        @command_arguments = command_arguments
      end

      def execute
        response_hash if command_arguments.present?
      end

      private

      attr_reader :command_arguments

      def response_hash
        return if invalid_date? || formatted_time.blank?

        {
          time:    formatted_time,
          date:    parsed_date.presence || DateTime.now.to_date,
          message: parsed_message
        }
      end

      def invalid_date?
        matched_date.present? && (parsed_date.blank? || !date_in_past_or_today?)
      end

      def date_in_past_or_today?
        parsed_date&.past? || parsed_date&.today?
      end

      def formatted_time
        strong_memoize(:formatted_time) do
          Gitlab::TimeTrackingFormatter.parse(extracted_time)
        end
      end

      def extracted_time
        command_arguments.gsub(matched_date.to_s, '')
                         .gsub(matched_message.to_s, '')
                         .gsub(USER_REGEX, '' )
                         .delete('"')
      end

      def parsed_message
        strong_memoize(:parsed_message) { matched_message.delete("'\"") }
      rescue
        nil
      end

      def matched_message
        strong_memoize(:matched_message) { match_data(MESSAGE_REGEX) }
      end

      def parsed_date
        strong_memoize(:parsed_date) { Date.parse(matched_date) }
      rescue
        nil
      end

      def matched_date
        strong_memoize(:matched_date) { match_data(DATE_REGEX) }
      end

      def match_data(regexp)
        matched_data = command_arguments.match(regexp)
        matched_data.try(:[], 0)
      end
    end
  end
end
